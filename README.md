# Лабораторна робота 2
## «Дослідження додаткових конструкцій в ООП з лямда виразами. Робота з колекціями та параметрізовані класи (generic)»
**Виконали:** Зубко Карина та Кієвць Іван

**Мета:** Навчитися налагоджувати оточення Java-розробника. Вивчити основи використання системи GIT
### Варіант 6:
Реализовать свертку множеста (произвольного конечного ряда чисел) используя попароное сумирование или сумирование одинаковвых по мощьности подмножест. 
Правила задаются функцией и должны быть варьируемы. 
Пример: на первом этапе суммируются попарно рядом стоящие числа, на втором этапе суммируются результаты первого этапа и т. д. до тех пор, пока не останется одно число. 

### Варіант 7:
Сложить два многочлена заданной степени, если коэффициенты многочленов хранятся в объекте HashMap.
Коэффициенты могут быть как целыми, дробными, или комплексные 