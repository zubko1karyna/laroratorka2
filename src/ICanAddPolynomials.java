@FunctionalInterface
public interface ICanAddPolynomials {

    Polynomial addPolynomials(Polynomial first, Polynomial second);
    default void showExample()
    {
        System.out.println();
        System.out.println("Example of adding polynomials:");

        System.out.println("2x + 1");
        System.out.println("+");
        System.out.println("2x + 1");
        System.out.println("-------");
        System.out.println("4x + 2");
        System.out.println();
    }
}
