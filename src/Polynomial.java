import java.util.HashMap;

public class Polynomial<T extends Comparable>
{
    private HashMap<Integer, T> _polynomial;
    private int _size;

    public Polynomial()
    {
        _polynomial = new HashMap();
        _size = 0;
    }

    public void setSize(int size){
        _size = size;
    }

    public int getSize(){
        return _size;
    }

    public <T> T getValueByKey(int key){
        return (T) _polynomial.get(key);
    }

    public void AddValue(int key, T value)
    {
        _polynomial.put(key, value);
    }

    public void PrintPolynomial()
    {
        _polynomial.forEach((key, value) -> System.out.println(key + " coefficient = " + value));
    }
}
