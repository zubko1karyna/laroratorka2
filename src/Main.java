import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ICanAddPolynomials solution = (first, second) -> {
            var result = new Polynomial();

            for(int key = 1; key <= first.getSize(); key++)
            {
                if(first.getValueByKey(key).getClass().getTypeName().equals(Complex.class.getTypeName()))
                {
                    result.AddValue(key, ((Complex)first.getValueByKey(key)).add((Complex)second.getValueByKey(key)));
                }else {
                    result.AddValue(key, (Double)first.getValueByKey(key) + (Double)second.getValueByKey(key));
                }
            }

            return result;
        };

        var scanner = new Scanner(System.in);

        var numberOfCoeffs = 0;
        var close = true;

        var polynomial_1 = new Polynomial();
        var polynomial_2 = new Polynomial();

        while (close)
        {
            try {
                ShowMenu();

                switch (Integer.parseInt(scanner.nextLine())) {
                    case 1:
                        System.out.println("Enter number of coefficients:");
                        numberOfCoeffs = Integer.parseInt(scanner.nextLine());

                        System.out.println("Enter 1 polynomial:");
                        InputPolynomial(polynomial_1, numberOfCoeffs, scanner);
                        System.out.println("Enter 2 polynomial:");
                        InputPolynomial(polynomial_2, numberOfCoeffs, scanner);

                        solution.addPolynomials(polynomial_1, polynomial_2).PrintPolynomial();
                        break;
                    case 2:
                        Complex.ShowFormatOfComplex();
                        break;
                    case 3:
                        solution.showExample();
                        break;
                    case 4:
                        calculations c = new calculations();
                        c.array();
                        int[] arr = c.generate(10, 5);
                        while (arr.length > 1) {
                            System.out.println(Arrays.toString(arr));
                            arr = c.modify(arr);
                            c.steps();
                        }
                        c.existingMethods(Arrays.toString(arr));
                        break;
                    case 0:
                        close = false;
                        break;
                    default:
                        throw new Exception("Invalid menu item");

                }
            }catch (Exception exception)
            {
                System.out.println(exception);
            }
        }
    }

    public static void InputPolynomial(Polynomial polynomial, int numberOfCoeffs, Scanner scanner)
    {
        for (int i = 1; i <= numberOfCoeffs; i++) {

            String inputTxt = scanner.nextLine();

            if(inputTxt.contains("i")){
                polynomial.AddValue(i, Complex.parseComplex(inputTxt));
            }else {
                polynomial.AddValue(i, Double.parseDouble(inputTxt.replace(",", ".")));
            }
        }

        polynomial.setSize(numberOfCoeffs);
    }

    public static void ShowMenu(){

        System.out.println("Menu:");
        System.out.println("1 - Add polynomials(task 7)");
        System.out.println("2 - Show format input complex number");
        System.out.println("3 - Show example of adding polynomials");
        System.out.println("4 - Pairwise summation(task 6)");
        System.out.println("0 - Close program");
    }
}