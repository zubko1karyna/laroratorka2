public class Complex implements Comparable<Complex>{

    private double _real;
    private double _imaginary;

    public Complex(double real, double imaginary){
        _real = real;
        _imaginary = imaginary;
    }

    public Complex add(Complex secondNum)
    {
        return new Complex(_real + secondNum._real, _imaginary + secondNum._imaginary);
    }

    public static Complex parseComplex(String str){

        String[] nums;
        double tempReal = 0;
        double tempImaginary = 0;

        if(str.endsWith("i"))
        {
            String strWithout_i = str.substring(0, str.length() - 1);

            nums = strWithout_i.split("\\+");

            tempReal = Double.parseDouble(nums[0].replace(",", "."));
            tempImaginary = Double.parseDouble(nums[1].replace(",", "."));
        }
        else if (str.endsWith("i)"))
        {
            String strWithout_i = str.substring(0, str.length() - 2);

            nums = strWithout_i.split("\\+\\(");

            tempReal = Double.parseDouble(nums[0].replace(",", "."));
            tempImaginary = Double.parseDouble(nums[1].replace(",", "."));
        }

        return new Complex(tempReal, tempImaginary);
    }

    public static void ShowFormatOfComplex(){
        System.out.println();
        System.out.println("Format of complex numbers:");
        System.out.println("4+5i");
        System.out.println("-4+5i");
        System.out.println("4+(-5i)");
        System.out.println("-4+(-5i)");
        System.out.println();
    }

    @Override
    public String toString(){
        if(_imaginary > 0)
            return _real + "+" + _imaginary + "i";
        else
            return _real + "+(" + _imaginary + "i)";
    }

    @Override
    public int compareTo(Complex o) {
        return 0;
    }
}
