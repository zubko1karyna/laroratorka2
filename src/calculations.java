import java.util.Arrays;
import java.util.Random;

public class calculations implements Sum{
    public static int[] modify(int[] arr) {
        int newSize = (int) Math.round(arr.length / 2.0);
        int[] arrAfterSum = new int[newSize];
        for (int i = 0; i < arr.length; i++) {
            arrAfterSum[i / 2] += arr[i];
        }
        return arrAfterSum;
    }
    public static int[] generate(int count, int maxNumber) {
        int[] arr = new int[count];
        java.util.Random rand = new java.util.Random();
        for (int i = 0; i < count; i++) {
            arr[i] = rand.nextInt(maxNumber + 1);
        }
        return arr;
    }

    @Override
    public void existingMethods(String s) {
        System.out.println("Sum = "+s);
    }
}
